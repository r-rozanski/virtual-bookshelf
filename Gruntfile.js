module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dev: {
                options: {
                    sourcemap: 'auto',
                    style: 'expanded'
                },
                files: {
                    'app/css/main.css': 'app/scss/main.scss'
                }
            }
        },
        express: {
            dev: {
                options: {
                    script: './server.js'
                }
            }
        },
        watch: {
            sass: {
                files: 'app/scss/{,*/}*.{scss,sass}',
                tasks: ['sass:dev']
            },
            express: {
                files: ['**/*.js'],
                tasks: ['express:dev'],
                options: {
                    spawn: false
                }
            }
        },
        copy: {
            build: {
                cwd: 'app',
                src: [ '**' ],
                dest: 'dist',
                expand: true
            }
        },
        clean: {
            build: {
                src: [ 'dist' ]
            },
            css: {
                src: ['dist/css/*.*', '!dist/css/style.css', 'dist/scss']
            },
            js: {
                src: ['dist/js/*.*', '!dist/js/code.js']
            },
            components: {
                src: ['dist/components']
            }
        },
        cssmin: {
            build: {
                files: {
                    'dist/css/style.css': [
                        'dist/components/normalize.css/normalize.css',
                        'dist/css/*.css'
                    ]
                }
            }
        },
        uglify: {
            build: {
                options: {
                    mangle: false
                },
                files: {
                    'dist/js/code.js': [
                        'dist/components/jquery/dist/jquery.js',
                        'dist/components/jquery-ui/jquery-ui.js',
                        'dist/components/underscore/underscore.js',
                        'dist/components/moment/moment.js',
                        'dist/components/angular/angular.js',
                        'dist/js/*.js'
                    ]
                }
            }
        },
        useminPrepare: {
            html: 'index.html',
            options: {
                flow: {}
            }
        },
        usemin: {
            html: 'dist/index.html'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-usemin');

    grunt.registerTask('server', ['express:dev', 'watch']);
    grunt.registerTask('build',[
        'useminPrepare',
        'clean:build',
        'copy',
        'cssmin',
        'uglify',
        'clean:css',
        'clean:js',
        'clean:components',
        'usemin'
    ]);
};
