var app = angular.module('VirtualBookshelf', ['controllers', 'services', 'ui.sortable']);

app.directive('showBookDetails', ['Books', function(Books) {
    return {
        link: function(scope, element) {
            element.bind('click', function() {
                var bookId = element.data('book-id');
                var lastBookInRow = function() {
                    var currentBook = element.parent().index();
                    var lastInRow = 4 + ((Math.floor(element.parent().index() / 5)) * 5);

                    for (var i = lastInRow; i >= currentBook; i--) {
                        if (angular.element('.book-cover').eq(i).length > 0) {
                            return i;
                        }
                    }
                };
                scope.setShelfHeights();

                Books.details(bookId, function(data) {
                    var book = data;
                    var detailsDiv = angular.element('.shelf-book-details');
                    var template = '<div class="shelf-book-details" data-book-id="'+book.id+'">' +
                            '<div class="left-side">' +
                                '<h2>'+book.title+'</h2>' +
                                '<h4>'+book.authors.toString().replace(/,/g, ", ")+'</h4>' +
                                '<ul>' +
                                    '<li>Genre: '+book.genre+'</li>' +
                                    '<li>Language: '+book.language+'</li>' +
                                    '<li>Purchase date: '+moment(book.purchase_date).format("MMMM D, YYYY")+'</li>' +
                                '</ul>' +
                            '</div>' +
                            '<div class="right-side">' +
                                '<p>'+book.description+'</p>' +
                            '</div>' +
                        '</div>';

                    if (detailsDiv.length > 0) {
                        if (detailsDiv.data('book-id') == book.id) {
                            scope.closeBookDetails();
                            if (scope.manualSorting) {
                                scope.sortableOptions.disabled = false;
                            }
                        } else {
                            scope.closeBookDetails();
                            scope.openBookDetails(lastBookInRow(), template);
                        }
                    } else {
                        scope.sortableOptions.disabled = true;
                        scope.openBookDetails(lastBookInRow(), template);
                    }
                });
            })
        }}
    }
]);

app.directive('setShelfInitialHeight', function () {
    return function (scope) {
        if (scope.$last) {
            scope.setShelfHeights();
            angular.element('.shelf-row').css('height', scope.shelfHeight + 'px');
        }
    };
});
