angular.module('controllers', [])
    .controller('MainListCtrl', ['$scope', 'Books', function($scope, Books) {
        $scope.sortableOptions = {
            'helper' : 'clone',
            'ui-floating': true,
            'disabled': true,
            stop: function() {
                window.localStorage['manualSortingOrder'] = angular.element('.shelf-row')
                    .sortable('toArray').toString();
            }
        };
        $scope.filterTitle = '';
        $scope.filterAuthor = '';
        $scope.filterGenre = 'All';
        $scope.filterLanguage = 'All';
        $scope.sortingOrder = 'title';
        $scope.manualSorting = false;

        Books.get(function (data) {
            $scope.booksFullList = data;
            $scope.books = $scope.booksFullList;
        });

        $scope.getListOfBooks = function (sortingOrder) {
            if ((sortingOrder === 'manual') && (window.localStorage['manualSortingOrder'].length > 0)) {
                $scope.manualSortingOrder = (window.localStorage['manualSortingOrder']).split(',');
                $scope.books = [];
                for (var k = 0; k < $scope.booksFullList.length; k++) {
                   $scope.books[parseInt($scope.manualSortingOrder
                       .indexOf($scope.booksFullList[k].id.toString()))] = $scope.booksFullList[k];
                }
            } else {
                $scope.books = _.sortBy($scope.booksFullList, sortingOrder);
                var tmpList = [];
                for (var i = 0; i < $scope.books.length; i++) {
                    var tmpAuthors = '';
                    for (var j = 0; j < $scope.books[i].authors.length; j++) {
                        tmpAuthors += $scope.books[i].authors[j] + ' ';
                    }

                    if ((($scope.books[i].title.toLowerCase().indexOf($scope.filterTitle.toLowerCase()) > -1) ||
                        $scope.filterTitle === '') &&
                        ((tmpAuthors.toLowerCase().indexOf($scope.filterAuthor.toLowerCase()) > -1) ||
                            $scope.filterAuthor === '') && ($scope.books[i].genre === $scope.filterGenre ||
                        $scope.filterGenre === 'All') && ($scope.books[i].language === $scope.filterLanguage ||
                        $scope.filterLanguage === 'All')) {
                        tmpList.push($scope.books[i]);
                    }
                }
                $scope.books = tmpList;
                $scope.booksNumber = $scope.books.length;
            }
        };

        // Get list of books for the first time after loading a page.
        $scope.getListOfBooks($scope.sortingOrder);

        $scope.setShelfHeights = function() {
            $scope.shelfHeight = 0;
            if ($scope.books.length % 5 === 0) {
                if ($scope.books.length === 0) {
                    $scope.shelfHeight = 230;
                } else {
                    $scope.shelfHeight = ($scope.books.length / 5) * 230;
                }
            }  else {
                $scope.shelfHeight = (Math.floor($scope.books.length / 5) + 1) * 230;
            }
            $scope.shelfWithDetailsHeight = $scope.shelfHeight + 460;
            angular.element('.shelf-row').css('height', $scope.shelfHeight + 'px');
        };

        $scope.filterBooks = function(sortingOrder) {
            $scope.closeBookDetails();
            $scope.getListOfBooks(sortingOrder);
            $scope.booksNumber = $scope.books.length;
            $scope.setShelfHeights();
        };

        $scope.openBookDetails = function(rowEndBook, content) {
            $scope.setShelfHeights();
            angular.element('.shelf-row').css('height', $scope.shelfWithDetailsHeight + 'px');
            angular.element('.book-cover').eq(rowEndBook).after(content);
            angular.element('html,body')
                .animate({scrollTop: angular.element('.shelf-book-details').offset().top - 230},'slow');
        };

        $scope.closeBookDetails = function() {
            $scope.setShelfHeights();
            angular.element('.shelf-book-details').remove();
            angular.element('.shelf-row').css('height', $scope.shelfHeight + 'px');
        };

        $scope.changeSorting = function(sortingMode) {
            if (sortingMode === 'manual') {
                $scope.sortableOptions.disabled = false;
                angular.element('.filterInput, .filterSelect').prop('disabled', true);
                angular.element('.filterElement').fadeTo('slow', 0.5);
                $scope.tmpSortingOrder = $scope.sortingOrder;
                $scope.tmpFilterTitle = $scope.filterTitle;
                $scope.tmpfilterAuthor = $scope.filterAuthor;
                $scope.tmpFilterGenre = $scope.filterGenre;
                $scope.tmpFilterLanguage = $scope.filterLanguage;
                $scope.sortingOrder = 'title';
                $scope.filterTitle = '';
                $scope.filterAuthor = '';
                $scope.filterGenre = 'All';
                $scope.filterLanguage = 'All';
                $scope.filterBooks('manual');
                $scope.sortingOrder = $scope.tmpSortingOrder;
                $scope.filterTitle = $scope.tmpFilterTitle;
                $scope.filterAuthor = $scope.tmpfilterAuthor;
                $scope.filterGenre = $scope.tmpFilterGenre;
                $scope.filterLanguage = $scope.tmpFilterLanguage;
            } else {
                $scope.sortableOptions.disabled = true;
                angular.element('.filterInput, .filterSelect').prop('disabled', false);
                angular.element('.filterElement').fadeTo('slow', 1.0);
                $scope.sortingOrder = sortingMode;
                $scope.filterBooks($scope.sortingOrder);
            }
        }
    }]);
