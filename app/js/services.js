angular.module('services', [])
    .factory('Books', ['$http', function ($http) {
        return {
            get: function(callback) {
                $http.get('http://localhost:3000/api/books')
                    .success(function(data) {
                        callback(data);
                    });
            },
            details: function(bookId, callback) {
                $http.get('http://localhost:3000/api/book/' + bookId)
                    .success(function (data) {
                        callback(data);
                    });
            }
        }
    }]);
