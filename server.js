// Express config
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);
app.use(express.static(__dirname + '/app'));


// sample data
var books = require('./sample_data/books.json');


// routes
app.get('/', function(req, res) {
    res.type('text/html');
    res.sendFile(__dirname + '/app/index.html');
});

app.get('/api/books', function(req, res) {
    res.send(books);
});

app.get('/api/book/:bookId', function(req, res) {
    var bookId = req.params.bookId;
    var bookDetails = null;

    for (var i = 0; i < books.length; i++) {
        if (books[i].id === parseInt(bookId)) {
            bookDetails = books[i];
        }
    }
    res.send(bookDetails);
});

app.use(function(req, res) {
    res.status(404);
    res.send('Error 404');
});

app.use(function(err, req, res) {
    console.error(err.stack);
    res.status(500);
    res.send('Error 500');
});


app.listen(app.get('port'), function() {
    console.log( 'Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.' );
});
